<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Caller;
use Faker\Generator as Faker;


$factory->define(Caller::class, function (Faker $faker) {
	$faker = Faker::create('es_PE');
    return [
  			'name'=>$faker->name;
           ];
});
